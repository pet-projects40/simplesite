# Simple-Site
## Futures
- Header slider with: text blocks, menu navigation;
- Popup-event on the two buttons;
- CTA block with data form;
- Slider with: counter, navigation, link bullet points;
- Calories calculator;
- Product section with: JS generation content from DB.json;
- Countdown timer;
- Popup-event when site is scrolled to end of page;

## Technical stack
- HTML
- CSS
- JS
## Libs and program 

Simple Site is currently extended with the following libs and local server.
Instructions on how to use them in your own application are linked below.

| Library | README |Git|
| ------ | ------ | ------ |
| Json-server |  [https://www.w3schools.com/js/js_json_server.asp] | [https://www.npmjs.com/package/json-server] |
| Webpack | [https://webpack.js.org/] | [https://github.com/webpack/webpack] |
| Localserver | [https://www.mamp.info/] | [https://documentation.mamp.info/en/MAMP-Windows/Installation/index.html] |

## Local server configuration
Go to directory /MAMP/htdocs and open terminal in this folder and type the command.

```sh
git clone git@gitlab.com:petprojects40/simplesite.git
cd simplesite 
```
Run the local server for example MAMP
![](https://documentation.mamp.info/en/MAMP-Windows/First-Steps/MAMP.png)
**Start Servers / Stop Servers** 
Start or stop the Apache/Nginx and MySQL services of MAMP.

## Instalation
Install the dependencies and devDependencies and start the server.

```sh
cd simplesite
npm install
```
Don't worry about warnings in terminal, don't try to fix that all :) If dependencies installation is ready, you can see new folder in main project directory name is 'node modules'. You can try to call npm install again if you are not sure about dependencies installation. 

## Development

Simplesite use  Webpack for fast developing.
Make a change in your file and instantaneously see your updates!

Open your favorite Terminal and run these commands.

First Tab:

```sh
npx webpack --watch
```
Second Tab:
```sh
npx json-server db.json
```

To stop process in tab press CTRL + C

## Download assets folder from google drive
[Assets folder download this](https://drive.google.com/drive/folders/1j2g_dbnYWEc74qt6w74543BTycBi4aBo?usp=share_link) - after download add this folder to project in main directory ./assets




